#!/usr/bin/env python

# imports
import sys
import sqlite3


from prompt_toolkit import prompt, PromptSession, HTML, ANSI
    
def main():
    session = PromptSession()
    while True:
        try: 
            text = session.prompt(HTML("<ansiblue>❯ </ansiblue>"))
        except KeyboardInterrupt:
            continue
        except EOFError:
            break
        else:
            print(text)