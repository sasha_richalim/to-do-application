# TO KNOW HOW TO USE TODO. READ THE FILE CALLED README.TXT

import sqlite3

conn = sqlite3.connect('tododb.sqlite')
cur = conn.cursor()

cur.execute('''
CREATE TABLE IF NOT EXISTS Todo
    (task TEXT , due_date TEXT, description TEXT, status TEXT)''')


inp = input('What do you want to do? ')

status = 'Not done'

if len(inp) < 1:
    inp = 'Show all tasks'

# CREATE NEW TASK:
if inp == 'Create new task':
    print('Please enter the task name, due_date, and description below')
    task = input('')
    task_name, due_date, description = task.split(',')
    # print(task_name, due_date, description)
    cur.execute('''
    INSERT INTO Todo (task, due_date, description, status) VALUES (?, ?, ?, ?)''', (task_name, due_date, description, status))

    conn.commit()

# SHOW ALL TASKS:
if inp == 'Show all tasks':
    tinp = input('How do you want to view the data - ')
    cur.execute('''
    SELECT * FROM Todo''')
    row = cur.fetchall()
    # print(row)
    if tinp == 'Normal':
        for ival in row:
            print(ival[0])

    if tinp == 'Full data':
        for ival in row:
            print('Task: ' + ival[0] + ' | ' + 'Due date: ' + ival[1] + ' | ' + 'Description: ' + ival[2] + ' | ' + 'Status: ' + ival[3])

    conn.commit()

# DELETING TASKS:
if inp.startswith('Delete'):
    count = -1
    task_to_del = ''
    line = inp.split()
    # print(line)
    for word in line:
        if word == 'all':
            # print('found')
            cur.execute('''
            DELETE FROM Todo''')

        if word == 'task':
            lst = line[2:]
            # print(lst)
            for word in lst:
                if len(lst) == 1:
                    task_to_del = lst[0]
                    # print(task_to_del)

                if len(lst) > 1:
                    count = count + 1
                    task_to_del = task_to_del + lst[count] + ' '
                    # print(task_to_del)

        cur.execute('DELETE FROM Todo WHERE task=?', (task_to_del,))

    conn.commit()

# TASK DONE:
if inp.endswith('done'):
    count = -1
    fintask = ''

    line = inp.split()
    # print(line)

    lst = line[:-1]
    # print(lst)
    for word in lst:
        if len(lst) == 1:
            fintask = lst[0]
            # print(fintask)

        if len(lst) > 1:
            count = count + 1
            fintask = fintask + lst[count] + ' '
            # print(fintask)

    cur.execute('''
    UPDATE Todo SET status='done' WHERE task=?''', (fintask,))

    conn.commit()

cur.close()
