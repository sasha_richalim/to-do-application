EVERYTHING SHOWN BELOW IS JUST FOR FUN

All of the below works have been licensed under SASHA PERSONAL WORK company (not
a real company).

Todo desktop application version 1.0

This todo application uses a database to store all the data of your tasks. The questions
you can query the application are listed below

  a) To create a new task, type in - Create new task
  b) To show all tasks, type in - Show all tasks
  c) To delete a particular task, type in - Delete task {task_name} - to delete all, type in - Delete all tasks
  d) If you have done a task already, then type in - {task_name} done

To open the Todo application, type the following lines into our terminal as shown below:

1. Debian/Linux
Type in the following in a new terminal window:
  a) cd {directory}/Todo/ver_1_0/
  b) python3 todo_ver_1_0.py
  c) And make any query you want

2. OS X
Type in the following in a new terminal window:
  a) cd {directory}/Todo/ver_1_0/
  b) python3 todo_ver_1_0.py
  c) And make any query you want

3. Windows
Type in the following in a new terminal window:
  a) cd /home/{directory}/Todo/ver_1_0/
  b) todo_ver_1_0.py
  c) And make any query you want

To improve the app contact me at sasha.richalim.che@gmail.com or call me at 9164713383
