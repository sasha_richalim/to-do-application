# UPDATES:
#   a) My Day feature;
#      with ability to refresh every time the user wants to
#   b) Ability to edit a certain attribute
#   c) New way of displaying data
#   d) More user friendly
#   e) Name feature
#   f) About section feature
#   g) New show feature

import sqlite3

conn = sqlite3.connect('tododb.sqlite')
cur = conn.cursor()

cur.executescript('''
CREATE TABLE IF NOT EXISTS Todo
    (id TEXT, task TEXT , due_date TEXT, description TEXT, status TEXT);

CREATE TABLE IF NOT EXISTS My_Day
    (id TEXT, task TEXT, due_time TEXT, description TEXT, status TEXT );

CREATE TABLE IF NOT EXISTS User_info
    (name TEXT)''')

# ASKING THE USER HIS/HER NAME:
cur.execute('''
SELECT * FROM User_info''')
row = cur.fetchone()

if row is None:
    print('Hi, what is your name?')
    ninp = input('>>> ')
    cur.execute('''
    INSERT INTO User_info (name) VALUES (?)''', (ninp,))
    # print(name)

if row is not None:
    cur.execute('''
    SELECT * FROM User_info''')
    row = cur.fetchone()
    # print(row[0])
    name = row[0]

    # NEW UPDATE: MORE USER FRIENDLY

    print('Hi', name)
    print('')

print('What do you want to do?')
inp = input('>>> ')


# STATUS CONSTANT:
status = 'Not done'

# DEFAULT OPERTION:
if len(inp) < 1:
    inp = 'Show all tasks'

# CREATE NEW TASK:
if inp == 'Create new task':
    print('Table name')
    table_name = input('>>> ')
    if table_name == 'My Day':
        print('Please enter the id, task name, due time, and description below')
        task = input('>>> ')
        # print(task)
        id, task_name, due_time, description = task.split(',')
        # print(id, task_name, due_time, description)
        cur.execute('''
        INSERT INTO My_Day (id, task, due_time, description, status) VALUES (?, ?, ?, ?, ?)''', (id, task_name, due_time, description, status))
        print(task_name, 'task added to todo with corresponding id:', id)

    if table_name == 'Todo':
        print('Please enter the id, task name, due date, and description below')
        task = input('>>> ')
        id, task_name, due_date, description = task.split(',')
        # print(task_name, due_date, description)
        cur.execute('''
        INSERT INTO Todo (id, task, due_date, description, status) VALUES (?, ?, ?, ?, ?)''', (id, task_name, due_date, description, status))
        print(task_name, 'task added to Todo with corresponding id:', id)

    conn.commit()

# SHOW USER INFO TABLE DATA:
if inp == 'Show user info data':
    cur.execute('''
    SELECT * FROM User_info''')
    row = cur.fetchone()
    # print(row)
    print('Name:', row[0])

# SHOW ALL TASKS:
if inp == 'Show all tasks':
    print('Enter the table name from which the data is to be displayed')
    tabinp = input('>>> ')
    if len(tabinp) < 1:
        tabinp = 'My Day'

    print('How do you want to view the data')
    tinp = input('>>> ')

    if tabinp == 'My Day':
        cur.execute('''
        SELECT * FROM My_Day''')
        row = cur.fetchall()
        # print(row)
        if tinp == 'Normal':
            for ival in row:
                print(ival[0], ival[1], ival[4])

        if tinp == 'Full data':
            for ival in row:
                print('Id: ' + ival[0] + ' | ' + 'Task: ' + ival[1] + ' | ' + 'Due time: ' + ival[2] + ' | ' + 'Description: ' + ival[3] + ' | ' + 'Status: ' + ival[4] + '\n' + '\n')

    if tabinp == 'Todo':
        cur.execute('''
        SELECT * FROM Todo''')
        row = cur.fetchall()
        # print(row)
        if tinp == 'Normal':
            for ival in row:
                print(ival[0], ival[1], ival[4])

        if tinp == 'Full data':
            for ival in row:
                print('Id: ' + ival[0] + ' | ' + 'Task: ' + ival[1] + ' | ' + 'Due date: ' + ival[2] + ' | ' + 'Description: ' + ival[3] + ' | ' + 'Status: ' + ival[4] + '\n' + '\n')

    conn.commit()

# DELETING TASKS:
if inp.startswith('Delete'):
    count = -1
    task_to_del = ''
    line = inp.split()
    # print(line)
    for word in line:
        if word == 'all':
            cur.execute('''
            DELETE FROM Todo''')

            print('All tasks deleted from Todo')

        if word == 'task':
            lst = line[2:]
            # print(lst)
            for word in lst:
                if len(lst) == 1:
                    task_to_del = lst[0]
                    # print(task_to_del)

                if len(lst) > 1:
                    count = count + 1
                    task_to_del = task_to_del + lst[count] + ' '
                    # print(task_to_del)

        cur.execute('DELETE FROM Todo WHERE task=?', (task_to_del,))

        conn.commit()

    for word in line:
        # Ex: Delete task ......g...... from My Day; Day @ line[-1]
        if word[-1] == 'Day':
            lst = line[2:-2]
            # print(lst)
            for word in lst:
                if len(lst) == 1:
                    task_to_del = lst[0]
                    # print(task_to_del)

                if len(lst) > 1:
                    count = count + 1
                    task_to_del = task_to_del + lst[count] + ' '
                    # print(task_to_del)

        cur.execute('DELETE FROM My_Day WHERE task=?', (task_to_del,))

    conn.commit()

# TASK IS DONE:
if inp.endswith('done'):
    count = -1
    fintask = ''

    print('Enter the table name')
    tdtinp = input('>>> ')

    if len(tdtinp) < 1:
        tdtinp = 'My Day'

    if tdtinp == 'My Day':
        line = inp.split()
        # print(line)
        lst = line[:-1]
        # print(lst)
        for word in lst:
            if len(lst) == 1:
                fintask = lst[0]
                # print(fintask)

                if len(lst) > 1:
                    count = count + 1
                    fintask = fintask + lst[count] + ' '
                    print(fintask)

        cur.execute('''
        UPDATE My_Day SET status='done' WHERE task=?''', (fintask,))

        print(fintask, 'task marked as done')

    if tdtinp == 'Todo':
        line = inp.split()
        # print(line)
        lst = line[:-1]
        # print(lst)
        for word in lst:
            if len(lst) == 1:
                fintask = lst[0]
                # print(fintask)

            if len(lst) > 1:
                count = count + 1
                fintask = fintask + lst[count] + ' '
                # print(fintask)

        cur.execute('''
        UPDATE Todo SET status='done' WHERE task=?''', (fintask,))

        print(fintask, 'task marked as done')

        conn.commit()

# REFRESHING MY DAY:
if inp.startswith('refresh') or inp.startswith('Refresh'):
    cur.execute('''
    DELETE FROM My_Day''')
    print('My Day has been refreshed')

    conn.commit()

# EDITING A PARTICULAR ATTRIBUTE:
if inp.startswith('edit') or inp.startswith('Edit'):
    print('Table name')
    tabinp = input('>>> ')
    if len(tabinp) < 1:
        tabinp = 'My Day'

    if tabinp == 'My Day':
        print('Attribute')
        attr = input('>>> ')
        if attr == 'task name':
            print('Please enter the id of the task')
            id = input('>>> ')

            print('Enter the new task name')
            ntn = input('>>> ')

            cur.execute('''
            UPDATE My_Day SET task = ? WHERE id = ?''', (ntn, id))

            cur.execute('''
            SELECT * FROM My_Day WHERE id = ?''', (id,))

            row = cur.fetchall()
            # print(type(row))
            # print(row)

            for ival in row:
                # print(ival)
                print('Task name set to -', ival[1])

        if attr == 'due time':
            print('Please enter the id of the task corresponding to the due time')
            id = input('>>> ')

            print('Enter the new due time')
            ndt = input('>>> ')

            cur.execute('''
            UPDATE My_Day SET due_time = ? WHERE id = ?''', (ndt, id))

            cur.execute('''
            SELECT * FROM My_Day WHERE id = ?''', (id,))

            row = cur.fetchall()
            # print(type(row))
            # print(row)

            for ival in row:
                # print(ival)
                print('Due time set to -', ival[2])

        if attr == 'description':
            print('Please enter the id of the task corresponding to the description')
            id = input('>>> ')

            print('Enter the new description')
            ndsc = input('>>> ')

            cur.execute('''
            UPDATE My_Day SET description = ? WHERE id = ?''', (ndsc, id))

            cur.execute('''
            SELECT * FROM My_Day WHERE id = ?''', (id,))

            row = cur.fetchall()
            # print(type(row))
            # print(row)

            for ival in row:
                # print(ival)
                print('Description set to -', ival[3])

    if tabinp == 'Todo':
        print('Attribute')
        attr = input('>>> ')
        if attr == 'task name':
            print('Please enter the id of the task')
            id = input('>>> ')

            print('Enter the new task name')
            ntn = input('>>> ')

            cur.execute('''
            UPDATE Todo SET task = ? WHERE id = ?''', (ntn, id))

            cur.execute('''
            SELECT * FROM Todo WHERE id = ?''', (id,))

            row = cur.fetchall()
            # print(type(row))
            # print(row)

            for ival in row:
                # print(ival)
                print('Task name set to -', ival[1])

        if attr == 'due time':
            print('Please enter the id of the task corresponding to the due time')
            id = input('>>> ')

            print('Enter the new due time')
            ndt = input('>>> ')

            cur.execute('''
            UPDATE Todo SET due_time = ? WHERE id = ?''', (ndt, id))

            cur.execute('''
            SELECT * FROM Todo WHERE id = ?''', (id,))

            row = cur.fetchall()
            # print(type(row))
            # print(row)

            for ival in row:
                # print(ival)
                print('Due time set to -', ival[2])

        if attr == 'description':
            print('Please enter the id of the task corresponding to the description')
            id = input('>>> ')

            print('Enter the new description')
            ndsc = input('>>> ')

            cur.execute('''
            UPDATE Todo SET description = ? WHERE id = ?''', (ndsc, id))

            cur.execute('''
            SELECT * FROM Todo WHERE id = ?''', (id,))
            row = cur.fetchall()

            # print(type(row))
            # print(row)

            for ival in row:
                # print(ival)
                print('Description set to -', ival[3])

    conn.commit()

# ABOUT SECTION:
if inp == 'About Todo':
    print('_______________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________')
    print('About Todo\n\nApp: Todo\nver: 1.2\n\nThe todo application is an open-source terminal based app\ncreated by a coder named Sasha Richalim Che. This application manages the\nusers tasks and does many more things.\nRead the file README.txt to learn more on how to use this application.\n\nUpdates rolled out in the current version:\n    a) My Day feature; with ability to refresh every time the user wants to\n    b) Ability to edit a certain attribute\n    c) New way of displaying data\n    d) More user friendly\n    e) Name feature\n    f) About section feature\n    g) New show feature\n\nIf you are interested in improving the app call me at 9164713383 or\nemail at sasha.richalim.che@gmail.com.\n\nHope you enjoy the app! ;-)  ')

cur.close()
