Todo desktop application version 1.2

This todo application uses a database to store all the data of your tasks. The questions
you can query the application are show below.

In the app there are two kinds of tables to enter the tasks in. The first one is the Todo table. The todo table is specialized to
keep your tasks for a long time, while the second one My Day table is specialized to manage your temporary tasks for each day.
(refresh My Day everyday before entering new task for the next day!) See how to use these tables in the 2nd paragraph.

To open the Todo application, type the following lines into our terminal as shown below based on the type of operation system (OS) you use:

1. Debian/Linux
Type in the following in a new terminal window:
  a) cd {directory}/Todo/ver_1_2/
  b) python3 todo_ver_1_2.py
  c) And make any query you want

2. OS X
Type in the following in a new terminal window:
  a) cd {directory}/Todo/ver_1_2/
  b) python3 todo_ver_1_2.py
  c) And make any query you want

3. Windows
Type in the following in a new terminal window:
  a) cd /home/{directory}/Todo/ver_1_2/
  b) python3 todo_ver_1_2.py
  c) And make any query you want

Here is the list of queries which you ask te application

1. CREATING TASKS
  a) To create a new task type in the commands as follows('>>>' means input and {} mean the input can be variable):
       Enter the command that you want to create a new task
       >>> Create new task
       Then type in the table name you want to create the task into
       >>> My Day or >>> Todo
       Then enter the task name, due date (or due time if you entered My Day in the table name input), and description
       >>> {id},{task name},{due date(YYYY-MM-DD) or time(HH:MM:SS)},{description}
           Ex: 1,Work,(due date: 2021-4-5) or (due time: 08:00:00),work
       Then the app will store the task and save it unles ou delete it

  b) To show all tasks from either My Day or Todo type as follows:
       Enter the command that you want to Show all tasks
       >>> Show all tasks
       Then type in the table name from which you want to retrieve the data from
       >>> My Day or >>> Todo
       Then enter how you want the data to be represented to you(normal mode shows you only the id, task and status while the full mode shows all the data)
       >>> Normal or >>> Full data
       And then the app will display all the tasks based on how you told to represent it

  c) To delete all tasks or a certain task in the Todo table type in as follows
       Enter the command that you want to delete all  task or a certain task
       >>> Delete all tasks or >>> Delete task {task name}
       To delete a specific task in My Day type in the following
       >>> Delete task {task name} from My Day
       Then enter the table name (note:- only deleting a specific task in My Day is supported to delete all tasks refresh my day )
       >>> Todo or >>> My Day (note as written above)
       And the task is deleted

  d) To mark a task as done in either Todo or My Day type in the following:
       >>> {task name} done
       Then enter the table name
       >>> Todo or >>> My Day
       And then the status is marked as done

  e) To refresh My Day type in as follows
       >>> refresh My day or >>> refresh or >>> Refresh
       And then My day will be refreshed

  f) To edit a particular attribute - values such as task name, due time, and description
       >>> edit attr or >>> Edit attr
       Then enter the table name
       >>> Todo or >>> My Day
       Then enter the attribute you want to edit
       >>> task name or >>> due time (due date if table is Todo) or >>> description
       And then enter the new value for the attribute

  g) To see all the specs of the Todo app then type in as follows
       >>> About Todo

And continue to work throughout your day!

If you want to improve the app contact me at sasha.richalim.che@gmail.com or call me at 9164713383

Hope you enjoy the app!
