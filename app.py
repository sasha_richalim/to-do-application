# imports
import sys
import sqlite3

from prompt_toolkit import print_formatted_text, ANSI, HTML
# application imports (in order of usuage)
import prompt # the prompt.py file


# welcome
def welcome():
    print_formatted_text(HTML('''<purple>Todo 2.14</purple> <ansigreen>(date, time)</ansigreen> on <ansigreen>(os)</ansigreen>
<i>Type "help" or "license" for more information.</i>\n'''))

# some setup
def setup():
    conn = sqlite3.connect("tododb.sqlite")
    cur = conn.cursor()

    cur.executescript('''
CREATE TABLE IF NOT EXISTS Inbox (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    task TEXT UNIQUE,
    due_date TEXT, description TEXT,
    tags TEXT
);

CREATE TABLE IF NOT EXISTS Today (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    task TEXT UNIQUE, due_time TEXT,
    description TEXT, tags TEXT
);

CREATE TABLE IF NOT EXISTS User_info (
    name TEXT
)
''')
    conn.commit()
    cur.close()

# welcome
welcome()
# the main prompt
if __name__ == "__main__":
    setup()
    prompt.main()